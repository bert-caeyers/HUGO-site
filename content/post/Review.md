+++
Categories = ["Engels","Stage"]
Description = "GIP-taak 13"
Tags = ["Post"]
date = "2016-05-07T14:50:11+02:00"
type = "post"
title = "Review"

+++

## Driver Genius

I'm going to review the online tool Driver Genius today. Driver Genius is a program that helps you keep track of your Windows drivers. You can use it for every pc that has a Windows operating system. You are probably asking yourself what drivers are, drivers are components of a computer that you connect with your motherboard. They can be internal but also peripheral like a mouse / computer screen  but also a cpu or gpu. These drivers are needed so the components are compatible with your operating system (windows). You could look them up by brand but that's a lot of work. I used this tool during my apprenticeship for a lot of things but mostly for cleaning up computers.



So why is this tool so useful? Well the most common problems with pcs are drivers that are not up-to-date. This tool keeps all the drivers that are needed for your pc up-to-date. What's good about this, is that it’s with one tool so you don't have to search every driver by brand. This saves you a lot of time and is really easy to use. The tool works on every pc that runs Windows. You also have other tools like this one Driver Support or Smart Driver Updater. But I can't give my opinion about the other ones because I only used Driver Genius. So how does the tool begins his work. It starts by searching for the different drivers. When it has found all the drivers it searches if the drivers must be updated. After this it asks you if it can install all the updates of the drivers. You then just click "Yes" and the tool starts installing all the updates.


What I think is good about this tool is that it's really easy to use. Everybody can use it because you just need to run the program and then click on two buttons. What's also really useful is that the tool has a big unique database of all the drivers. So if you want you can search them yourself. This is also the strongest point pointed out by the review I read. Driver Genius is the only tool for drivers that has this. What's also really good is that you don't have to worry about the third party developers.


Of course it doesn't only have good features. I think it's a bit too expensive if you just want to use it for your home pc. I also read other reviews and they said it only found half of the updates. The most not found updates were peripheral. But I have to disagree with this negative point because when I used it I always found all the updates. Over the two weeks I used it I didn't find two updates. So I think it's a really good tool.





## Conclusion:
8/10

I'm really positive about this tool because it's easy to use. It also found most of the updates when I used it. The tool also has a really nice interface and the database is so helpful.

I think it's mostly good for big companies or companies with a lot of pcs. This is because like I already said most of the pc problems are with drivers that are not up to date and a lot of companies depend on their computers nowadays. So if that problem is solved that would be fantastic for companies and for these companies it's only a small price.

I wouldn't advise small companies or people at home to use it, because paying so much for just three computers is not worth it.
