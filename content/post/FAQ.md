+++
Categories = ["Frans"]
Description = "GIP-taak 04"
Tags = ["Development", "golang"]
date = "2016-05-07T14:48:30+02:00"
type = "post"
title = "FAQ"

+++

## Foire Aux Questions

Vous aimez les quiz? Kahoot est votre truc. Vous pouvez utiliser Kahoot pour tout. Un test dans la classe ou juste simplement pour le plaisir. Pour des vieux ou des jeunes, c'est pour tout le monde.


1. Est-ce qu’il y a une application de Kahoot?
  * Oui, il y a une application pour Android et Apple. Vous devez seulement visiter le Google Play Store ou bien l’Apple Store et chercher ‘Kahoot’.
2. Est-ce que je peux faire un quiz pour mes élèves?
  * Oui, c’est possible. Nous vous recommandons de visiter notre blog: http://blog.getkahoot.com/post/49502843173/from-learners-to-leaders-with-the-kahoot-pedagogy. Une autre possibilité est que vos étudiants s’inscrivent sur notre site.
3. Pourrez- vous voir votre résultat?
  * Oui, chaque fois après un quiz vous voyez le résultat des participant(e)s. Vous voyez aussi qui a gagné le quiz. C’est la personne avec les meilleurs points.
4. Sur quels appareils pouvez-vous utiliser kahoot?
  * Vous pouvez créer un quiz seulement sur l’ordinateur. Mais vous pouvez jouer les quiz sur trois appareils: l’ordinateur, la tablette et le gsm.
5. Est-ce que vous pouvez faire un quiz avec l’application ou vous pouvez jouer  uniquement?
  * Vous pouvez seulement jouer avec l’application. Parce que c’est trop difficile à créer un quiz avec l’application.
6. Est-ce que c’est gratuit?
  * Oui, Kahoot est gratuit. Si vous voulez donner de l’argent, c’est possible sur notre site.
