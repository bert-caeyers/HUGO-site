+++
Categories = ["Software"]
Description = "GIP-taak 08"
Tags = ["Post"]
date = "2016-05-07T14:47:34+02:00"
type = "post"
title = "Database"

+++

![foto1](../../Afbeeldingen/diagram.png)

## Inhoud:

Mijn plan was om een database te maken waarop leerlingen met hun leerkrachten kunnen communiceren en hun lessenroosters kunnen nazien. Dit idee is gebaseerd op de al bestaande website smartschool.
Elke leerling heeft een eigen unieke account waarop zijn lessenrooster staat. Op deze lessenrooster staat niet alleen het vak maar ook de leerkracht die het geeft. De leerlingen en leerkrachten kunnen ook met elkaar communiceren dit doen ze aan de hand van een bericht te sturen met een onderwerp meegegeven.

## SQL Code:

> ### Database en tabellen maken

>create schema gip_taak_08;                                                                                                                            
use gip_taak_08;

>create table Leerling(                                                                                                       
	id int not null auto_increment primary key,                                                                                   
    Naam text not null,                                                                                                                                   
    Woonplaats text not null,                                                                                                                                       
    klas text not null,                                                                                                             
    graad text not null                                                                                                                 
);

>create table Leerkracht(                                                                                                                               
	id int not null auto_increment primary key,                                                                                                                  
    Naam text not null,                                                                                                                                     
    Woonplaats text not null,                                                                                                                                        
    vak text not null,                                                                                                                                
    graad text not null                                                                                                                                     
);

>create table berichten(                                                                                                                                                    
	id int not null auto_increment primary key,                                                                                                                                        
    Leerkracht_id int,                                                                                                                                    
    Leerling_id int,                                                                                                                                            
    Onderwerp text not null                                                                                                                 
    );

>create table vak(                                                                                                                                                
	id int not null auto_increment primary key,                                                                                                                                
    Leerling_id int,                                                                                                                                          
    Leerkracht_id int                                                                                                                                                     
      );

> ### Gegevens invoeren

>insert into Leerling(id, Naam, Woonplaats, klas, graad)                                                                                                                                                               
values                                                                                                                                                                                      
(1, "Maarten", "Sint-Antonius", "6IB", "3de"),                                                                                                                                  
(2, "Wout", "Malle","6IB", "3de"),                                                                                                                                                
(3, "Ricardo", "Brecht","6IB", "3de");                                                                                                                   


>insert into Leerkracht(id, Naam, Woonplaats, vak, graad)                                                                                                                                       
values                                                                                                                                                                                  
(1, "Degrauwe", "Wijnegem", "Geschiedenis", "3de"),                                                                                                                                       
(2, "Vanbergen", "Geel", "Nederlands", "3de");                                                                                                                                                            

>insert into vak(id, Leerling_id, Leerkracht_id)                                                                                                                                    
values                                                                                                                                                                          
(1,1,1),                                                                                                                                                                          
(2,3,1),                                                                                                                                                                                          
(3,2,2),                                                                                                                                                                                            
(4,1,2),                                                                                                                                                                                            
(5,3,2),                                                                                                                                                                                        
(6,2,1);                                                                                                              

>insert into berichten(id, Leerling_id, Leerkracht_id, Onderwerp)                                                                                                                                     
values                                                                                                                                                                                                      
(1,2,2, "Test"),                                                                                                                                                                                  
(2,1,1,"Werkstudie");

> ### Gegevens opvragen

> #### Toon alle Leerlingen met hun vakken en hun leerkrachten

>SELECT *                                                                                                                                                                                               
FROM Leerling                                                                                                                                                                                         
JOIN vak ON Leerling.id = vak.Leerling_id                                                                                                                                                         
JOIN Leerkracht ON Leerkracht_id = Leerkracht.id;  																																																																		                                                                                                                                                   

![uitkomst1](../../Afbeeldingen/uitkomst2.png)


> #### Toon alle Leerlingen met alle berichten naar de leerkracht geschiedenis


>SELECT *                                                                                                                                                                                     
FROM Leerling                                                                                                                                                                             
JOIN berichten ON Leerling.id = berichten.Leerling_id                                                                                                                             
JOIN Leerkracht ON Leerkracht_id = Leerkracht.id                                                                                                                                  
WHERE Leerkracht.vak = "Geschiedenis";            																																																			                                                                                                                                                      

![uikomst2](../../Afbeeldingen/uitkomst1.png)
