+++
Categories = ["IT-tapa"]
Description = "GIP-taak 01"
Tags = ["Post"]
date = "2016-05-07T14:28:34+02:00"
type = "post"
title = "IT-tapa filmpjes"

+++

## Nederlandstalig filmpje

<iframe width="560" height="315" src="https://www.youtube.com/embed/S7AEDx58VIc" frameborder="0" allowfullscreen></iframe>

## Engelstalig filmpje

<iframe width="560" height="315" src="https://www.youtube.com/embed/pjFtuMjKIIA" frameborder="0" allowfullscreen></iframe>
