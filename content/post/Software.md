+++
Categories = ["Software"]
Description = "GIP-taak "
Tags = ["Post"]
date = "2016-05-08T12:23:38+02:00"
type = "post"
title = "Software"


+++


## WebsiteWorkflows

  Deze site is speciaal gemaakt voor het plaatsen van al mijn GIP-taken.
  Hier werd ook opzich een GIP-taak van gemaakt.
  Ik vind het zelf een fantastische ervaring om zelf een site te kunnen maken.
  Dit doen we wel met behulp van HUGO.
  HUGO is een tool die je helpt een site te maken door gebruik van verschillende partials.

## HUGO eigen mening

  Ik vind HUGO eigenlijk een zeer goede tool.
  In het begin is het wel even moeilijk maar eens je het doorhebt gaat het zeer goed.
  Wat ik minder vind is dat niet alle themas compatibel zijn om posts aan toe te voegen zoals de vorige versie van mijn site.
  Gelukkig lukt het nu bij mijn huidige site wel.
  Wat ik ook zeer leuk vind is dat je makkelijk verschillende themas samen kan gebruiken.
  Ik zou het zeker aanraden aan andere mensen maar zou de klemtoon leggen op extra uitleg bij het hoofdstuk partials en benadrukken om zeker alles op de site zeer goed na te lezen.
  Voor de rest is HUGO redelijk makkelijk te gebruiken.
  Soms ondervin je mogelijk wat weerstand maar met een portie doorzettingsvermogen lukt het zeker.
  Als ik zou moeten kiezen tussen Wordpress of HUGO zou mijn keuze ongetwijfeld naar deze laatste uitgaan, omdat je veel meer opties hebt en je de site naar je eigen stijl kan aanpassen.


## Doelen met de site

Ik ben zeker nog van plan een ander thema toe te voegen.
Dan ga ik gebruik maken van posts in plaats van alles in de HTML code te verranderen.
Voor de rest is alles naar wens verlopen.
Maar ik ben nog wel van plan om zeer veel aan de site te verranderen.
