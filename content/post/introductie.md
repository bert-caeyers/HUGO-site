+++
author = ""
categories = []
date = "2016-06-21T17:45:24+02:00"
description = ""
featured = ""
featuredalt = ""
featuredpath = ""
linktitle = ""
title = "Introductie"

+++

Welkom op mijn GIP-site. Mijn naam is Bert Caeyers. Ik woon in België in de gemeente Schilde. Ik ben 17 jaar oud en ik volg de richting Informaticabeheer aan het Immaculata Instituut. Ik ben 12 jaar lang voetballer geweest en 1 jaar lang trainer. Ik ga regelmatig samen met vrienden uit en game ook wel wat in mijn vrije tijd. Ik kijk ook zeer graag series en films. Op dit moment heb ik nog geen grote toekomst plannen behalve mijn middelbare school afmaken en een richting gaan volgen op de hogeschool. Op dit moment zou ik verder willen gaan met hardware en systeembeheer maar mijn mening kan nog altijd door het jaar veranderen.

Deze site vormt een onderdeel van mijn geïntegreerde proef, kort gezegd mijn GIP. De tool HUGO stond centraal in deze opdracht. Dit is een tool die verschillende basispagina’s weergeeft op een website. Deze kan je dan gebruiken en aan je eigen stijl aanpassen. Het doel van deze site is alle taken, welke onderdeel vormen van deze GIP, online weer te geven. Momenteel zijn slechts een beperkt aantal taken raadpleegbaar. Door het dynamische verloop van deze GIP zal u merken dat een regelmatige update zal volgen. Ik hoop dat de site zichzelf uitwijst en u geen problemen tegenkomt. Als dit zou zijn kan u mij altijd contacteren. Ik wens jullie alvast veel leesplezier en ik hoop dat jullie genieten van mijn site.
