+++
author = ""
categories = ["Stage"]
date = "2016-05-09T16:45:44+02:00"
description = "GIP-taak"
featured = ""
featuredalt = ""
featuredpath = ""
linktitle = ""
title = "Dagverslagen"

+++

## Introductie

Hieronder vindt u alle verslagen die ik zo goed als dagelijks heb gemaakt op mijn stagebedrijf. In elk verslag staat er wat ik die dag heb gedaan.


### Dag 1 : 11-01-2016

>Vandaag heb ik mogen meewerken bij de helpservice. Hierbij heb ik mensen hun pc virusvrij moeten maken en ook nog mensen die hun wachtwoord niet meer weten die moeten helpen. Ik heb ook 2 pc's moeten installeren van het begin dus alle drivers en basisprogramma's moeten instaleren. Dit vond ik als eerste dag een geweldige ervaring. Ik heb ook al zeer veel bijgeleerd over malware en virussen. Mijn stagebegeleider en collega's waren ook zeer vriendelijk en zeker bereid mij te helpen als ik een probleem had. Eerste dag was dus zeker geslaagd!

### Dag 2 : 12-01-2016

>Vandaag heb ik terug mee gewerkt bij de restoratieservice. Ik heb een pc helemaal mogen in elkaar steken en installeren. Ik heb ook bij een oude pc alle drivers up to date mogen zetten en installeren. Ik heb ook vandaag zeer veel pc's moeten leegmaken en terug windows en office moeten op zetten. Dit deed ik met behulp van een usb. Voor de rest heb ik gewoon mensen moeten helpen en pc's moeten opkuizen ( van virussen). Over het algemeen een zeer toffe maar vermoeiende dag.

### Dag 3 : 13-01-2016

>Vandaag heb ik het internet netwerk bij iemand zijn thuis moeten fixen. Hiermee bedoel ik de ip-adressen van de verschillende acces points aanpassen zodat alles perfect zou werken. Ik heb ook nog bij een gemaakte site moeten zoeken of er een bug in de code zat. Het werkte perfect dus geen bug gevonden. Ik heb ook een laptop opengevezen en daarvan de harde schijf moeten clonen. De geclonde harde schijf moest ik dan terug verbinden met de laptop en dan het systeem laten rebooten.

### Dag 4 : 14-01-2016

>Vandaag heb ik bij een laptop de hdd-schijf moeten vervangen door een ssd-schijf. Deze heb ik moeten clonen en dan in de laptop moeten steken. Ik heb ook een pc helemaal in elkaar moeten steken. Ik heb ook voor de rest pc's en tablets van windows 7 naar windows 10 moeten upgraden.

### Dag 5 : 15-01-2016

>Vandaag was het zeer rustig en heb ik vooral de projecten waar ik de vorige dagen mee bezig was moeten afmaken. Ik heb ook nog een deel geholpen met de verhuis. Voor de rest heb ik gewoon een fan moeten vervangen van een laptop en deze terug moeten dicht lijmen. Dit moest ik doen omdat de case van de laptop niet gemaakt was om eigenlijk uit elkaar te halen. Toch moest ik dit doen omdat de fan stuk was en dus vervangen moest worden. De oplossing was dus met een lijmpistool de kleine hoeken terug vast lijmen en zo was het wel gelukt.

### Dag 6 : 18-01-2016

>Vandaag was het rustig, ik heb vooral in de namiddag alleen gestaan en dus ook mensen moeten bedienen als ze hulp nodig hadden. Dit was allemaal vlekkeloos gegaan en het was een leuke ervaring. Voor de rest heb ik een paar pc moeten formatteren en  de drivers daarvan terug up to date brengen. Ik heb ook moeten nachecken of sommige harde schijven nog wel werkte of niet. Dit heb ik gedaan met behulp van een tool. Het was allemaal over het algemeen een leuke dag met wel veel wachten maar dit kon ik invullen met andere kleine taakjes.

### Dag 7 : 19-01-2016

>Vandaag was het enorm rustig. Het koude weer heeft volgens mij een groot effect op het aantal klanten. Ik heb gewoon nog wat harde schijven moeten nakijken en ik heb ook een groot deel kunnen werken aan mijn GIP-taken. Voor de rest zat de sfeer wel goed tussen iedereen.

### Dag 9 : 21-01-2016

>Vandaag was het een zeer informatieve dag. Ik heb bij 2 laptops de schermen moeten verwisselen. Ik begrijp nu waarom mensen liever voor een desktop gaan. Voor de rest heb ik bij een paar pc's de upgrade moeten doen naar windows 10. Ik heb ook wat mensen moeten helpen bij de support lijn. Dan moet ik telefonisch mensen helpen en als dat niet genoeg is moet ik hun pc overpakken met hun eigen vorm van teamviewer. Het spijt me trouwens dat ik gisteren geen verslag heb gemaakt maar het was ongeveer dezelfde dag als de dag ervoor. Voor de rest heb ik vandaag aan mijn eigen laptop mogen werken. Het scherm ervan was kapot en ik heb door mijn werkgever een zeer goedkope oplossing kunnen krijgen voor het maken van mijn laptop. Ik ben hem echt enorm dankbaar.
