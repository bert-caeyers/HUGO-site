+++
Categories = ["IT-tapa"]
Description = "GIP-taak 02"
Tags = ["Post"]
date = "2016-05-07T14:48:13+02:00"
type = "post"
title = "It-tapa Middagsessie"

+++

## Middagsessie

* <a href = "https://www.facebook.com/IT-tapa-588405757863108/?fref=ts" target = "_blank">Facebookpagina</a>      
* <a href = "http://maarten-vissers.github.io/" target = "_blank" > Maarten Vissers</a>


Tijdens de IT-Tapa middagsessie hebben Maarten en ik meer uitleg gegeven over de online tool genaamd <a href= "https://getkahoot.com/">Kahoot</a>. Kahoot is een tool waarmee je online een quiz kan maken. Hier kan je dan in de klas gebruike van maken als je dat wilt en je kan het ook als een test gebruiken omdat je de uitslagen van de test kan afdrukken. Wat kahoot zo sterk maakt is dat de quiz met verschillende apparaten kan beantwoord worden, bv met een smartphone. Om het allemaal samen te vatten kan de tool Kahoot zeer handig zijn in het klaslokaal.

![middagsessie1](https://scontent-ams2-1.xx.fbcdn.net/hphotos-xfp1/t31.0-8/11231871_938939142809766_1985633652632721364_o.jpg)
